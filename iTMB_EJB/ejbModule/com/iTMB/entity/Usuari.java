package com.iTMB.entity;

import java.io.Serializable;
import javax.persistence.*;

@Entity
@Table(name="usuari")
public class Usuari implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	private String alias;

	private String passwd;

	public Usuari() {
		super();
		
		this.alias = "";
		this.passwd = "";
	}

	public String getAlias() {
		return this.alias;
	}

	public void setAlias(String alias) {
		this.alias = alias;
	}

	public String getPasswd() {
		return this.passwd;
	}

	public void setPasswd(String passwd) {
		this.passwd = passwd;
	}

	/*
	public Dadesusr getDadesusr() {
		return this.dadesusr;
	}

	public void setDadesusr(Dadesusr dadesusr) {
		this.dadesusr = dadesusr;
	}
	*/

}