package com.iTMB.entity;

import java.io.Serializable;
import javax.persistence.*;

@Entity
@Table(name="tiptranscmp")
@NamedQuery(name="getTransComp", query="SELECT a FROM Tiptranscmp AS a WHERE (a.id.nom1 = : pstrans1 AND a.id.nom2 = : pstrans2)  OR (a.id.nom1 = : pstrans2 AND a.id.nom2 = : pstrans1) ")
public class Tiptranscmp implements Serializable {
	private static final long serialVersionUID = 1L;

	@EmbeddedId
	private TiptranscmpPK id;

	public Tiptranscmp() {
	}

	public TiptranscmpPK getId() {
		return this.id;
	}

	public void setId(TiptranscmpPK id) {
		this.id = id;
	}

}