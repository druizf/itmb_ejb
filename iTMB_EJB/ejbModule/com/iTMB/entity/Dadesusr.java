package com.iTMB.entity;

import java.io.Serializable;
import javax.persistence.*;


/**
 * The persistent class for the dadesusr database table.
 * 
 */
@Entity
@Table(name="dadesusr")
@NamedQuery(name="getDadesusr", query="SELECT a FROM Dadesusr AS a WHERE a.usuari.alias = : psAlias")
public class Dadesusr implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	private String correu;

	private String cognoms;

	private int datanai;

	private String nom;

	private String sexe;

	private String telf;

	//bi-directional one-to-one association to Usuari
	@OneToOne
	@JoinColumn(name="alias")
	private Usuari usuari;

	public Dadesusr() {
		super();
		
		this.correu = "";
		this.cognoms = "";
		this.datanai = 0;
		this.nom = "";
		this.sexe = "";
		this.telf = "";
		
		this.usuari = new Usuari();
	}

	public String getCorreu() {
		return this.correu;
	}

	public void setCorreu(String correu) {
		this.correu = correu;
	}

	public String getCognoms() {
		return this.cognoms;
	}

	public void setCognoms(String cognoms) {
		this.cognoms = cognoms;
	}

	public int getDatanai() {
		return this.datanai;
	}

	public void setDatanai(int datanai) {
		this.datanai = datanai;
	}

	public String getNom() {
		return this.nom;
	}

	public void setNom(String nom) {
		this.nom = nom;
	}

	public String getSexe() {
		return this.sexe;
	}

	public void setSexe(String sexe) {
		this.sexe = sexe;
	}

	public String getTelf() {
		return this.telf;
	}

	public void setTelf(String telf) {
		this.telf = telf;
	}

	public Usuari getUsuari() {
		return this.usuari;
	}

	public void setUsuari(Usuari usuari) {
		this.usuari = usuari;
	}
	
	public String toString(){
		String sretValue = null;
		
		sretValue = "Alias: "         + this.usuari.getAlias() + "\n" +
					"Nom "            + this.nom               + "\n" +
					"Cognoms "        + this.cognoms           + "\n" +
					"Data Naixement " + this.datanai           + "\n" +
					"Sexe "           + this.sexe              + "\n" +
					"Telefon "        + this.telf;
		
		return sretValue;
	}

}