package com.iTMB.entity;

import java.io.Serializable;
import javax.persistence.*;

@Entity
@Table(name="tblsystem")
@NamedQuery(name="getvalue", query="SELECT a FROM Tblsystem AS a WHERE a.clau = : psclau")
public class Tblsystem implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	private String clau;

	private String valor;

	public Tblsystem() {
	}

	public String getClau() {
		return this.clau;
	}

	public void setClau(String clau) {
		this.clau = clau;
	}

	public String getValor() {
		return this.valor;
	}

	public void setValor(String valor) {
		this.valor = valor;
	}

}