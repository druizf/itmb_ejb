package com.iTMB.entity;

import java.io.Serializable;
import javax.persistence.*;


@Entity
@Table(name="tiptrans")
@NamedQueries({
	@NamedQuery(name="getListTrans", query="SELECT a FROM Tiptran AS a"),
	@NamedQuery(name="getTrans", query="SELECT a FROM Tiptran AS a WHERE a.nom = : pstrans")
})
public class Tiptran implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	private String nom;

	public Tiptran() {
	}

	public String getNom() {
		return this.nom;
	}

	public void setNom(String nom) {
		this.nom = nom;
	}

}