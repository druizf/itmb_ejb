package com.iTMB.entity;

import java.io.Serializable;
import javax.persistence.*;


@Entity
@Table(name="tipzona")
@NamedQueries({
	@NamedQuery(name="getListZones", query="SELECT a FROM Tipzona AS a"),
	@NamedQuery(name="getZona", query="SELECT a FROM Tipzona AS a WHERE a.zona = : pszona")
})
public class Tipzona implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	private byte zona;

	public Tipzona() {
	}

	public byte getZona() {
		return this.zona;
	}

	public void setZona(byte zona) {
		this.zona = zona;
	}

}