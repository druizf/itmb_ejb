package com.iTMB.entity;

import java.io.Serializable;
import javax.persistence.*;
import java.sql.Time;
import java.util.Date;

@Entity
@Table(name="relusrT11")
@NamedQueries({
@NamedQuery(name="getListT11OF", query="SELECT a FROM RelusrT11 AS a WHERE a.sit = 'OF'"),
@NamedQuery(name="updateDM", query="UPDATE RelusrT11 a set a.aliasRCP= ?1, " +
		"												   a.transRcp=?2, " +
		"												   a.latRcp= ?3, " +
		"												   a.lngRcp= ?4, " +
		"												   a.sit= ?5 " +
		"				 								   WHERE a.id= ?6"),
@NamedQuery(name="updateTR", query="UPDATE RelusrT11 a set a.sit= 'TR' WHERE a.id= ?1"),
@NamedQuery(name="updateCN", query="UPDATE RelusrT11 a set a.sit= 'OF' WHERE a.id= ?1"),
@NamedQuery(name="updateOB", query="UPDATE RelusrT11 a set a.sit= 'OB' WHERE a.id= ?1"),
@NamedQuery(name="getListT11EMS", query="SELECT a FROM RelusrT11 AS a WHERE a.aliasEMS = ?1"),
@NamedQuery(name="getListT11RCP", query="SELECT a FROM RelusrT11 AS a WHERE a.aliasRCP = ?1")
})
public class RelusrT11 implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	private int id;

	private String aliasEMS;

	private String aliasRCP;

	@Temporal(TemporalType.DATE)
	@Column(name="DataVld")
	private Date dataVld;

	@Column(name="HoraVld")
	private Time horaVld;

	@Column(name="LatEmi")
	private double latEmi;

	@Column(name="LatRcp")
	private double latRcp;

	@Column(name="LngEmi")
	private double lngEmi;

	@Column(name="LngRcp")
	private double lngRcp;

	private String sit;

	private String transEmi;

	private String transRcp;

	private byte zonaEmi;
	
	private String text;

	public RelusrT11() {
	}

	public int getId() {
		return this.id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getAliasEMS() {
		return this.aliasEMS;
	}

	public void setAliasEMS(String aliasEMS) {
		this.aliasEMS = aliasEMS;
	}

	public String getAliasRCP() {
		return this.aliasRCP;
	}

	public void setAliasRCP(String aliasRCP) {
		this.aliasRCP = aliasRCP;
	}

	public Date getDataVld() {
		return this.dataVld;
	}

	public void setDataVld(Date dataVld) {
		this.dataVld = dataVld;
	}

	public Time getHoraVld() {
		return this.horaVld;
	}

	public void setHoraVld(Time horaVld) {
		this.horaVld = horaVld;
	}

	public double getLatEmi() {
		return this.latEmi;
	}

	public void setLatEmi(double latEmi) {
		this.latEmi = latEmi;
	}

	public double getLatRcp() {
		return this.latRcp;
	}

	public void setLatRcp(double latRcp) {
		this.latRcp = latRcp;
	}

	public double getLngEmi() {
		return this.lngEmi;
	}

	public void setLngEmi(double lngEmi) {
		this.lngEmi = lngEmi;
	}

	public double getLngRcp() {
		return this.lngRcp;
	}

	public void setLngRcp(double lngRcp) {
		this.lngRcp = lngRcp;
	}

	public String getSit() {
		return this.sit;
	}

	public void setSit(String sit) {
		this.sit = sit;
	}

	public String getTransEmi() {
		return this.transEmi;
	}

	public void setTransEmi(String transEmi) {
		this.transEmi = transEmi;
	}

	public String getTransRcp() {
		return this.transRcp;
	}

	public void setTransRcp(String transRcp) {
		this.transRcp = transRcp;
	}

	public byte getZonaEmi() {
		return this.zonaEmi;
	}

	public void setZonaEmi(byte zonaEmi) {
		this.zonaEmi = zonaEmi;
	}
	
	public String getText() {
		return text;
	}

	public void setText(String text) {
		this.text = text;
	}
	
}