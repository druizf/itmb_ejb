package com.iTMB.entity;

import java.io.Serializable;
import javax.persistence.*;

@Entity
@Table(name="prova")
@NamedQuery(name="getuser", query="SELECT a FROM Prova AS a WHERE a.alias =?1 AND a.passwd = PASSWORD(?2)")
public class Prova implements Serializable {
	private static final long serialVersionUID = 1L;

	private String alias;

	@Lob
	private String passwd;

	public Prova() {
	}

	public String getAlias() {
		return this.alias;
	}

	public void setAlias(String alias) {
		this.alias = alias;
	}

	public String getPasswd() {
		return this.passwd;
	}

	public void setPasswd(String passwd) {
		this.passwd = passwd;
	}

}