package com.iTMB.entity;

import java.io.Serializable;
import javax.persistence.*;


@Entity
public class T11infousr implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	private String alias;

	private int numofr;

	private int numrsvAcp;

	private int numrsvCan;

	private int numtrt;

	public T11infousr() {
	}

	public String getAlias() {
		return this.alias;
	}

	public void setAlias(String alias) {
		this.alias = alias;
	}

	public int getNumofr() {
		return this.numofr;
	}

	public void setNumofr(int numofr) {
		this.numofr = numofr;
	}

	public int getNumrsvAcp() {
		return this.numrsvAcp;
	}

	public void setNumrsvAcp(int numrsvAcp) {
		this.numrsvAcp = numrsvAcp;
	}

	public int getNumrsvCan() {
		return this.numrsvCan;
	}

	public void setNumrsvCan(int numrsvCan) {
		this.numrsvCan = numrsvCan;
	}

	public int getNumtrt() {
		return this.numtrt;
	}

	public void setNumtrt(int numtrt) {
		this.numtrt = numtrt;
	}

}