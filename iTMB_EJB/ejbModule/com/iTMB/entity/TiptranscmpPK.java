package com.iTMB.entity;

import java.io.Serializable;
import javax.persistence.*;

@Embeddable
public class TiptranscmpPK implements Serializable {
	//default serial version id, required for serializable classes.
	private static final long serialVersionUID = 1L;

	private String nom1;

	private String nom2;

	public TiptranscmpPK() {
	}
	public String getNom1() {
		return this.nom1;
	}
	public void setNom1(String nom1) {
		this.nom1 = nom1;
	}
	public String getNom2() {
		return this.nom2;
	}
	public void setNom2(String nom2) {
		this.nom2 = nom2;
	}

	public boolean equals(Object other) {
		if (this == other) {
			return true;
		}
		if (!(other instanceof TiptranscmpPK)) {
			return false;
		}
		TiptranscmpPK castOther = (TiptranscmpPK)other;
		return 
			this.nom1.equals(castOther.nom1)
			&& this.nom2.equals(castOther.nom2);
	}

	public int hashCode() {
		final int prime = 31;
		int hash = 17;
		hash = hash * prime + this.nom1.hashCode();
		hash = hash * prime + this.nom2.hashCode();
		
		return hash;
	}
}