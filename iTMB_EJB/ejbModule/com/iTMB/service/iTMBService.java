package com.iTMB.service;

import javax.ejb.Remote;
import javax.ejb.Stateless;
import javax.ejb.TransactionAttribute;
import javax.ejb.TransactionAttributeType;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.PersistenceContextType;

import com.iTMB.bean.DadesusrBean;
import com.iTMB.bean.ErrorBean;
import com.iTMB.bean.ListRelUsrT11Bean;
import com.iTMB.bean.ListT11UsrInfoBean;
import com.iTMB.bean.TipTransBean;
import com.iTMB.bean.ZonaBean;
import com.iTMB.bean.iTMBInterfaceService;

@Stateless
@Remote(iTMBInterfaceService.class)
public class iTMBService implements iTMBInterfaceService{
 
	@PersistenceContext(type=PersistenceContextType.TRANSACTION)
	private EntityManager manager;
	
	@TransactionAttribute(TransactionAttributeType.REQUIRED)
	public ErrorBean validar_Usuari(String psAlias, String psPasswd){
		return UsuariService.validar_Usuari(psAlias, psPasswd, manager);
	}
	     
	@TransactionAttribute(TransactionAttributeType.REQUIRED)
	public ErrorBean crear_Usuari(String psAlias, String psPasswd, String psCorreu, String psNom, String psCognoms, int piDatanai, String psSexe, String psTelf) {
		return UsuariService.crear_Usuari(psAlias, psPasswd, psCorreu, psNom, psCognoms, piDatanai, psSexe, psTelf, manager);
	}

	@TransactionAttribute(TransactionAttributeType.REQUIRED)
	public ErrorBean modificar_Usuari(String psAlias, String psPasswd, String psCorreu, String psNom, String psCognoms, int piDatanai, String psSexe, String psTelf){
		return UsuariService.modificar_Usuari(psAlias, psPasswd, psCorreu, psNom, psCognoms, piDatanai, psSexe, psTelf, manager);
	}
	
	@TransactionAttribute(TransactionAttributeType.REQUIRED)
	public DadesusrBean obtenir_dades_Usuari(String psAlias){
		return UsuariService.obtenir_dades_Usuari(psAlias, manager);
	}
	
	@TransactionAttribute(TransactionAttributeType.REQUIRED)
	public ErrorBean esborrar_Usuari(String psAlias){
		return UsuariService.esborrar_Usuari(psAlias, manager);
	}

	@TransactionAttribute(TransactionAttributeType.REQUIRED)
	public ZonaBean obtenir_Zones() {
		return TransportsService.obtenir_Zones(manager);
	}
	
	@TransactionAttribute(TransactionAttributeType.REQUIRED)
	public ErrorBean es_Zona(String psZona) {
		return TransportsService.es_Zona(psZona, manager);
	}

	@TransactionAttribute(TransactionAttributeType.REQUIRED)
	public TipTransBean obtenir_Tip_Transports() {
		return TransportsService.obtenir_Tip_Transports(manager);
	}
	
	@TransactionAttribute(TransactionAttributeType.REQUIRED)
	public ErrorBean es_Tip_Transports(String psTrans) {
		return TransportsService.es_Tip_Transports(psTrans, manager);
	}
	
	@TransactionAttribute(TransactionAttributeType.REQUIRED)
	public ErrorBean es_trans_compatible(String psTrans1, String psTrans2) {
		return TransportsService.es_trans_compatible(psTrans1, psTrans2, manager);
	}

	@TransactionAttribute(TransactionAttributeType.REQUIRED)
	public ErrorBean oferir_T11(String psAlias, String psTrans, String psZona,
			String psDataFnl, String psHoraFnl, double pdLat, double pdLng, String psMiss) {
		return T11Service.oferir_T11(psAlias, psTrans, psZona, psDataFnl, psHoraFnl, pdLat, pdLng, psMiss, manager);
		
	}

	@TransactionAttribute(TransactionAttributeType.REQUIRED)
	public ListT11UsrInfoBean disponibles_T11(String psTrans, double pdLat, double pdLng) {
		return T11Service.disponibles_T11(psTrans, pdLat, pdLng, manager);
	}

	@TransactionAttribute(TransactionAttributeType.REQUIRED)
	public ErrorBean reservar_T11(int piIDRel, String psAlias, String psTrans, double pdLat, double pdLng) {
		return T11Service.reservar_T11(piIDRel, psAlias, psTrans, pdLat, pdLng, manager);
	}

	@TransactionAttribute(TransactionAttributeType.REQUIRED)
	public ErrorBean cancelar_T11(int piIDRel, String psAlias) {
		return T11Service.cancelar_T11(piIDRel, psAlias, manager);
	}

	@TransactionAttribute(TransactionAttributeType.REQUIRED)
	public ErrorBean confirmar_T11(int piIDRel, String psAlias) {
		return T11Service.confirmar_T11(piIDRel, psAlias, manager);
	}
	
	@TransactionAttribute(TransactionAttributeType.REQUIRED)
	public ListRelUsrT11Bean Llistat_T11_Emissor(String psAlias) {
		return T11Service.Llistat_T11_Emissor(psAlias, manager);
	}
	
	@TransactionAttribute(TransactionAttributeType.REQUIRED)
	public ListRelUsrT11Bean Llistat_T11_Receptor(String psAlias){
		return T11Service.Llistat_T11_Receptor(psAlias, manager);
	}
	
}
