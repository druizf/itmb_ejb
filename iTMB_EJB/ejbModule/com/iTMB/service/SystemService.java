package com.iTMB.service;

import javax.persistence.NoResultException;
import javax.persistence.Query;
import javax.persistence.EntityManager;

import com.iTMB.entity.Tblsystem;

public class SystemService {
		     
	  
	 public static String rec_valor(String psClau, EntityManager poEm){
		 String sRet = null;
		 		 
		 try 
		 { 
			Query oQuery = poEm.createNamedQuery("getvalue");
			oQuery.setParameter("psclau", psClau);
			
			Tblsystem oTblsystem = (Tblsystem) oQuery.getSingleResult();
			
			sRet = oTblsystem.getValor();
			
		 } catch (NoResultException e) { }
		 
		 return sRet;
	 }	
}
