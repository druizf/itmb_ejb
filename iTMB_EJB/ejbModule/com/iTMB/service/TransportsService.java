package com.iTMB.service;

import java.util.Iterator;
import java.util.List;

import javax.persistence.NoResultException;
import javax.persistence.Query;
import javax.persistence.EntityManager;

import com.iTMB.bean.ErrorBean;
import com.iTMB.bean.TipTransBean;
import com.iTMB.bean.ZonaBean;
import com.iTMB.entity.Tiptran;
import com.iTMB.entity.Tiptranscmp;
import com.iTMB.entity.Tipzona;

public class TransportsService {
		     
	 public static ZonaBean obtenir_Zones(EntityManager poEm){
		 ZonaBean oZonaBean = null;
		 ErrorBean oErrorBean = null;
		 
		 try 
		 { 
			Query oQuery = poEm.createNamedQuery("getListZones");
			
			@SuppressWarnings("rawtypes")
			List loZona = oQuery.getResultList();
			
			oZonaBean = new ZonaBean(loZona.size());
			
			@SuppressWarnings("rawtypes")
			Iterator  itZona = loZona.iterator();
						
			Tipzona oTipZona = null;
			int i = 0;
			
			while (itZona.hasNext()){
				oTipZona = (Tipzona) itZona.next();
				oZonaBean.setZona(i, Byte.toString(oTipZona.getZona()));
				i++;
			}
			
		 } catch (NoResultException e) {
			oZonaBean = new ZonaBean(0);
			
			oErrorBean = new ErrorBean("SQL0001", "No existeixen zones de transport en el sistema");
			oZonaBean.setErrorBean(oErrorBean);
			
		 }
		  
		 return oZonaBean;
	 }
	 
	 public static ErrorBean es_Zona(String psZona, EntityManager poEm){
		 ErrorBean oErrorBean = null;
		 		 
		 try 
		 { 
			Query oQuery = poEm.createNamedQuery("getZona");
			oQuery.setParameter("pszona", Byte.valueOf(psZona).byteValue());
			
			@SuppressWarnings("unused")
			Tipzona oZona = (Tipzona) oQuery.getSingleResult();
			oErrorBean = new ErrorBean();
			
		 } catch (NoResultException e) {
			oErrorBean = new ErrorBean("VLD0001", "No existeix la zona introduida");
		 }
		 
		 return oErrorBean;
	 }
	  
	 public static TipTransBean obtenir_Tip_Transports(EntityManager poEm){
		 TipTransBean oTipTransBean = new TipTransBean();
		 ErrorBean oErrorBean = null;
		 
		 try 
		 { 
			Query oQuery = poEm.createNamedQuery("getListTrans");
			
			@SuppressWarnings("rawtypes")
			List loTipTrans = oQuery.getResultList();
			
			@SuppressWarnings("rawtypes")
			Iterator  itTiptrans = loTipTrans.iterator();
						
			Tiptran oTipTran = null;
			
			while (itTiptrans.hasNext()){
				oTipTran = (Tiptran) itTiptrans.next();
				oTipTransBean.setTipTrans(oTipTran.getNom());
			}
			
		 } catch (NoResultException e) {
			oErrorBean = new ErrorBean("SQL0002", "No existeixen Tipus de transports definits en el sistema");
			oTipTransBean.setErrorBean(oErrorBean);
			
		 }
		  
		 return oTipTransBean;
	 }
	 
	 public static ErrorBean es_Tip_Transports(String psTrans, EntityManager poEm){
		 ErrorBean oErrorBean = null;
		 
		 try 
		 {
			Query oQuery = poEm.createNamedQuery("getTrans");
			oQuery.setParameter("pstrans", psTrans);
			
			@SuppressWarnings("unused")
			Tiptran oTransCmp = (Tiptran) oQuery.getSingleResult();
			oErrorBean = new ErrorBean();
			
		 } catch (NoResultException e) { 
			 oErrorBean = new ErrorBean("VLD0005", "No existeix el tipus de transport introduït");
		 }
		 
		 
		 return oErrorBean;
	 }
	 
	 public static ErrorBean es_trans_compatible(String psTrans1, String psTrans2, EntityManager poEm){
		 ErrorBean oErrorBean = null;
		 
		 try 
		 {
			Query oQuery = poEm.createNamedQuery("getTransComp");
			oQuery.setParameter("pstrans1", psTrans1);
			oQuery.setParameter("pstrans2", psTrans2);
			
			@SuppressWarnings("unused")
			Tiptranscmp oTransCmp = (Tiptranscmp) oQuery.getSingleResult();
			oErrorBean = new ErrorBean();
			
		 } catch (NoResultException e) {
			 oErrorBean = new ErrorBean("VLD0004", "Transports no compatibles");
		 }
		 
		 return oErrorBean;
	 }
	
}
