package com.iTMB.service;

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

import javax.persistence.NoResultException;
import javax.persistence.Query;
import javax.persistence.EntityManager;

import com.iTMB.bean.DadesusrBean;
import com.iTMB.bean.ErrorBean;
import com.iTMB.entity.Dadesusr;
import com.iTMB.entity.Usuari;

public class UsuariService {
		    
	 public static ErrorBean validar_Usuari(String psAlias, String psPasswd, EntityManager poEm){
		 ErrorBean oErrorBean = null;
		 String sPasswd = null; 
		 
		 Usuari oUsuari = poEm.find(Usuari.class, psAlias);
		 if(oUsuari != null){
			 sPasswd = oUsuari.getPasswd();
			 
			 MessageDigest oMes = null;
			 String sPasswdEnc = null;
			 try {
				oMes = MessageDigest.getInstance("SHA-256");
				oMes.update(psPasswd.getBytes());
				byte[] result = oMes.digest();
				sPasswdEnc = bytesToHex(result); 
			 } catch (NoSuchAlgorithmException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			 }
			 
			 boolean bPasswd = sPasswd.compareTo(sPasswdEnc) == 0;
			 if (bPasswd) oErrorBean = new ErrorBean();
			 else oErrorBean = new ErrorBean("VLD0002", "Usuari correcte. Password incorrecte");
		 }else{
			  oErrorBean = new ErrorBean("VLD0001", "Usuari inexistent");
		 }
		 
		 return oErrorBean;
	 }    
	     	 	 	 
	 public static ErrorBean crear_Usuari(String psAlias, String psPasswd, String psCorreu, String psNom, String psCognoms, int piDatanai, String psSexe, String psTelf,  EntityManager poEm) {
		 ErrorBean oErrorBean = null;
		 
		 Usuari oUsuari = poEm.find(Usuari.class, psAlias);
		 Dadesusr oDadesusr = poEm.find(Dadesusr.class, psCorreu);
		 
		 if(oUsuari == null) {
			 if (oDadesusr == null) {
			 
				 try 
				 {
					 oUsuari = new Usuari();
					 oDadesusr = new Dadesusr();
					 
					 oUsuari.setAlias(psAlias);
					 
					 // Encriptar el password
					 MessageDigest oMes;
					 oMes = MessageDigest.getInstance("SHA-256");
					 oMes.update(psPasswd.getBytes());
					 byte[] result = oMes.digest();
					 
					 String sPasswdEnc = bytesToHex(result);
				
					 
					 oUsuari.setPasswd(sPasswdEnc);
					 
					 // Omplir les dades del usuari
					 oDadesusr.setCorreu(psCorreu);
					 oDadesusr.setNom(psNom);
					 oDadesusr.setCognoms(psCognoms);
					 oDadesusr.setDatanai(piDatanai);
					 oDadesusr.setSexe(psSexe);
					 oDadesusr.setTelf(psTelf);
					 
					 poEm.persist(oUsuari);
					 
					 oDadesusr.setUsuari(oUsuari);
				 
					 poEm.persist(oDadesusr);
					
					 oErrorBean = new ErrorBean();
				
				} catch (Exception e) {
					oErrorBean = new ErrorBean(e.getCause().toString(),e.getMessage());
				}
			 } else oErrorBean = new ErrorBean("VLD0003", "Correu ja existent");
		 }else oErrorBean = new ErrorBean("VLD0004", "Usuari ja existent");
		 
		 return oErrorBean;
	 }
	 
	 public static ErrorBean modificar_Usuari(String psAlias, String psPasswd, String psCorreu, String psNom, String psCognoms, int piDatanai, String psSexe, String psTelf,  EntityManager poEm){
		 ErrorBean oErrorBean = null;
		 
		 Usuari oUsuari = poEm.find(Usuari.class, psAlias);
		 if (oUsuari != null) {
			 Query oQuery = poEm.createNamedQuery("getDadesusr");
			 oQuery.setParameter("psAlias", psAlias);
			 Dadesusr oDadesusr = (Dadesusr) oQuery.getSingleResult();
			 
			 if (oDadesusr != null) {
				 
				 if (psPasswd.compareTo("") != 0) oUsuari.setPasswd(psPasswd);
				 if (psCorreu.compareTo("") != 0) oDadesusr.setCorreu(psCorreu);
				 if (psNom.compareTo("") != 0) oDadesusr.setNom(psNom);
				 if (psCognoms.compareTo("") != 0) oDadesusr.setCognoms(psCognoms);
				 if (piDatanai != 0) oDadesusr.setDatanai(piDatanai);
				 if (psSexe.compareTo("") != 0) oDadesusr.setSexe(psSexe);
				 if (psTelf.compareTo("") != 0) oDadesusr.setTelf(psTelf);
				 
				 poEm.refresh(oUsuari);
				 poEm.refresh(oDadesusr);
				 poEm.flush();
				 
				 oErrorBean = new ErrorBean();
			 } oErrorBean = new ErrorBean("VLD0004", "Dades de l'usuari introduït, no trobats");
		 } else  oErrorBean = new ErrorBean("VLD0001", "Usuari inexistent"); 
		 	 
		 return oErrorBean;
	 }
	 
	 public static DadesusrBean obtenir_dades_Usuari(String psAlias, EntityManager poEm){
		 DadesusrBean oDadesusrBean = null;
		 ErrorBean oErrorBean = null;
		 
		 try 
		 {
			Query oQuery = poEm.createNamedQuery("getDadesusr");
			oQuery.setParameter("psAlias", psAlias);
			Dadesusr oDadesusr = (Dadesusr) oQuery.getSingleResult();
			Usuari oUsuari = oDadesusr.getUsuari();
			
			oDadesusrBean = new DadesusrBean(oUsuari.getAlias(), oUsuari.getPasswd(), oDadesusr.getCorreu(), oDadesusr.getCognoms(), oDadesusr.getDatanai(), oDadesusr.getNom(), oDadesusr.getSexe(), oDadesusr.getTelf(), new ErrorBean());
			
		 } catch (NoResultException e) {
			oErrorBean = new ErrorBean("SQL0001", "No existeix cap usuari amb aquest nom");
			oDadesusrBean = new DadesusrBean();
			oDadesusrBean.setErrorBean(oErrorBean);
			
		 }
		  
		 return oDadesusrBean;
		 
	 }
	 
	 public static ErrorBean esborrar_Usuari(String psAlias, EntityManager poEm){
		 ErrorBean oErrorBean = null;
		 DadesusrBean oDadesusrBean = null;
		
		 try 
		 {
			Query oQuery = poEm.createNamedQuery("getDadesusr");
			oQuery.setParameter("psAlias", psAlias);
			Dadesusr oDadesusr = (Dadesusr) oQuery.getSingleResult();
			Usuari oUsuari = poEm.find(Usuari.class, psAlias);
			
			poEm.remove(oUsuari);
			poEm.remove(oDadesusr);
			poEm.flush();
			 
			oErrorBean = new ErrorBean();
			
		 } catch (NoResultException e) {
			oErrorBean = new ErrorBean("SQL0001", "No existeix cap usuari amb aquest nom");
			oDadesusrBean = new DadesusrBean();
			oDadesusrBean.setErrorBean(oErrorBean);
			
		 }
		 
		 return oErrorBean;		 
	 }
	 
	 public static ErrorBean validar_Usuari(String psAlias, EntityManager poEm){
		 ErrorBean oErrorBean = new ErrorBean();
	
		 Usuari oUsuari = poEm.find(Usuari.class, psAlias);
		 if(oUsuari == null){
		   oErrorBean.setCodi("VLD0001");
		   oErrorBean.setDescripcio("Usuari inexistent");
		 }
		
		 return oErrorBean; 
	 }
	 
	 private static String bytesToHex(byte[] b) {
	      char hexDigit[] = {'0', '1', '2', '3', '4', '5', '6', '7',
	                         '8', '9', 'A', 'B', 'C', 'D', 'E', 'F'};
	      StringBuffer buf = new StringBuffer();
	      for (int j=0; j<b.length; j++) {
	         buf.append(hexDigit[(b[j] >> 4) & 0x0f]);
	         buf.append(hexDigit[b[j] & 0x0f]);
	      }
	      return buf.toString();
	   }

}
