package com.iTMB.service;

import java.sql.Time;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.Iterator;
import java.util.List;

import javax.persistence.EntityManager;

import javax.persistence.NoResultException;
import javax.persistence.Query;

import com.iTMB.bean.ErrorBean;
import com.iTMB.bean.ListRelUsrT11Bean;
import com.iTMB.bean.ListT11UsrInfoBean;
import com.iTMB.bean.RelusrT11Bean;
import com.iTMB.bean.T11UsrInfoBean;
import com.iTMB.entity.RelusrT11;
import com.iTMB.entity.T11infousr;

public class T11Service {
	
	 private static String SitT11_OFERIDA  =  "OF"; 
	 private static String SitT11_DEMANADA =  "DM";
		     
	 @SuppressWarnings("deprecation")
	 public static ErrorBean oferir_T11( String psAlias, 
			 				            String psTrans,
			 					        String psZona,
			 					        String psDataFnl,
			 					        String psHoraFnl,
			 					        double pdLat,
			 					        double pdLng,
			 					        String psMiss,
			 					        EntityManager poEm){
		 ErrorBean oErrorBean = null;
		 		 
		 // Validar el Alias del usuari
		 oErrorBean = UsuariService.validar_Usuari(psAlias, poEm);
		 
		 // Validar el origen del transport agafat
		 if ((oErrorBean != null) && (oErrorBean.getCodi().compareTo("") == 0))
			oErrorBean = TransportsService.es_Tip_Transports(psTrans, poEm);
			
		 // Validar la zona escollida
		 if ((oErrorBean != null) && (oErrorBean.getCodi().compareTo("") == 0))
			oErrorBean = TransportsService.es_Zona(psZona, poEm);
		
		 // Validar les dates introduides
		 if ((oErrorBean != null) && (oErrorBean.getCodi().compareTo("") == 0))
			 oErrorBean = es_datesvld(psDataFnl, psHoraFnl);
		 
		 // Es recupera el número màxim de donacions T11 per dia que es pot fer i es comprova
		 /*
		 if ((oErrorBean != null) && (oErrorBean.getCodi().compareTo("") == 0)){
			 int iNumOfer = Integer.valueOf(SystemService.rec_valor("numofer", poEm)).intValue();
		 }
		 */
		 
		 // Si no hi ha cap error, es procedeix a gravar el registre
		 if ((oErrorBean != null) && (oErrorBean.getCodi().compareTo("") == 0)){
			 String sDataTmp = null;
			 Time oTime = null;
			 
			 RelusrT11 oRelusrT11 = new RelusrT11();
			 
			 oRelusrT11.setAliasEMS(psAlias);
			 oRelusrT11.setLatEmi(pdLat);
			 oRelusrT11.setLngEmi(pdLng);
			 oRelusrT11.setTransEmi(psTrans);
			 oRelusrT11.setZonaEmi(Byte.valueOf(psZona).byteValue());
			 oRelusrT11.setSit(SitT11_OFERIDA);
			 
			 // Tenir en compte que el format s'ha de passar com MM/DD/YYYY
			 sDataTmp = Integer.valueOf(psDataFnl.substring(4, 6)).intValue() + "/" +
			 			Integer.valueOf(psDataFnl.substring(6, 8)).intValue() + "/" + 
			 			Integer.valueOf(psDataFnl.substring(0, 4)).intValue();
					 		
			 oTime = new Time(Integer.valueOf(psHoraFnl.substring(0, 2)), 
			                  Integer.valueOf(psHoraFnl.substring(2, 4)),
				              00); 
			
			 oRelusrT11.setDataVld(new Date(sDataTmp));
			 oRelusrT11.setHoraVld(oTime);
			 oRelusrT11.setText(psMiss);
			 
			 poEm.persist(oRelusrT11);		 
			 
			 // Es grava en el registre d'informació de l'usuari
			 mod_T11infousr(psAlias, true, false, false, false, poEm);
			 oErrorBean = new ErrorBean();
		 }
		 
		 return oErrorBean;
	 }
	 
	 private static ErrorBean es_datesvld(String psDataFnl, String psHoraFnl){
		 ErrorBean oErrorBean = null;
		 int iHora;
		 int iMin;
		 SimpleDateFormat dateFormat = new SimpleDateFormat("yyyyMMdd");
		 dateFormat.setLenient(false);
		 	 
		 // Validar la data de finalització
		 if (oErrorBean == null) {
			 if (psDataFnl.compareTo(" ") != 0){ 
				 String sDataTmp = psDataFnl.substring(0, 4) + psDataFnl.substring(4, 6) + psDataFnl.substring(6, 8);
				 try {		
					 dateFormat.parse(sDataTmp.trim());  
				 }  
				 catch (Exception pe) {  
					 oErrorBean = new ErrorBean("VLD0005", "La data de finalització no és correcte"); 
				 }  
				 
			 } else oErrorBean = new ErrorBean("VLD0004", "La data de finalització ha d'estar informada");
		 }
		 
		 if (oErrorBean == null) {
			 if (psHoraFnl.compareTo(" ") != 0){
				iHora = Integer.valueOf(psHoraFnl.substring(0, 2));
				iMin =  Integer.valueOf(psHoraFnl.substring(2, 4));
				if ((iHora > 24) || (iMin > 60)) 
					oErrorBean = new ErrorBean("VLD0004", "La hora de finalització no és correcte");
				 
			 } else oErrorBean = new ErrorBean("VLD0004", "La hora de finalització ha d'estar informada");
		 }	 
		 
		 /*
		 // Validar si la data de finalització és mes gran que la data d'inici
		 if (oErrorBean == null) {
			 boolean bRet = true;
			 
			 int    sDataTmpAnyini = Integer.valueOf(psDataini.substring(0, 4)).intValue();
			 int    sDataTmpAnyFnl = Integer.valueOf(psDataFnl.substring(0, 4)).intValue();
			 int    sDataTmpMesini = Integer.valueOf(psDataini.substring(4, 6)).intValue();
			 int    sDataTmpMesFnl = Integer.valueOf(psDataFnl.substring(4, 6)).intValue();
			 int    sDataTmpDiaini = Integer.valueOf(psDataini.substring(6, 8)).intValue();
			 int    sDataTmpDiaFnl = Integer.valueOf(psDataFnl.substring(6, 8)).intValue();
			 int    iHoraTmpini = Integer.valueOf(psHoraini.substring(0, 2));
			 int    iMinTmpini = Integer.valueOf(psHoraini.substring(2, 4));
			 int    iHoraTmpFnl = Integer.valueOf(psHoraFnl.substring(0, 2));
			 int    iMinTmpFnl = Integer.valueOf(psHoraFnl.substring(2, 4));
			 
			 if (sDataTmpAnyFnl < sDataTmpAnyini) bRet = false;
			 else if (sDataTmpAnyFnl == sDataTmpAnyini){
				 if (sDataTmpMesFnl <  sDataTmpMesini) bRet = false;
				 else if (sDataTmpMesFnl == sDataTmpMesini){
					 if (sDataTmpDiaFnl < sDataTmpDiaini) bRet = false;
					 else if (sDataTmpDiaFnl == sDataTmpDiaini){
						 if (iHoraTmpFnl < iHoraTmpini) bRet = false;
						 else if (iHoraTmpFnl == iHoraTmpini){
							 if (iMinTmpFnl < iMinTmpini) bRet = false; 
						 }
					 }
				 }
			 }
			 
			 if (!bRet) oErrorBean = new ErrorBean("VLD0008", "La data de finalització no pot ser inferior a la data d'inici");
			 
		 }
		 */
		 
		 if (oErrorBean == null) oErrorBean = new ErrorBean();
			 
		 return oErrorBean;
	 }
	 
	 public static ListT11UsrInfoBean disponibles_T11( String psTrans,
			 				     	                   double pdLat,
			 				     	    	           double pdLng,
			 				     	    	           EntityManager poEm){
		 ListT11UsrInfoBean oListT11UsrInfoBean = null;
		 ErrorBean oErrorBean = null;
		 		
		 oErrorBean = TransportsService.es_Tip_Transports(psTrans, poEm);
		 if ((oErrorBean != null) && (oErrorBean.getCodi().compareTo("") == 0))
			 oListT11UsrInfoBean = recuperar_T11_disponibles(psTrans, pdLat, pdLng, poEm);
		 else {
			 oListT11UsrInfoBean = new ListT11UsrInfoBean();
			 oListT11UsrInfoBean.setoErrorBean(oErrorBean);
		 }
		 			 
		 return oListT11UsrInfoBean;
	 }

	 private static ListT11UsrInfoBean recuperar_T11_disponibles(String psTrans,
   		   								     		             double pdLat,
   		   								     		             double pdLng,
   		   								     		             EntityManager poEm){
		 
		 ListT11UsrInfoBean oListT11UsrInfoBean = new ListT11UsrInfoBean();
		 ErrorBean oErrorBean = null;
		 
		 try 
		 { 
			Query oQuery = poEm.createNamedQuery("getListT11OF");
			
			@SuppressWarnings("rawtypes")
			List loRelusrT11 = oQuery.getResultList();
				
			@SuppressWarnings("rawtypes")
			Iterator  itRelusrT11 = loRelusrT11.iterator();
						
			RelusrT11 oRelusrT11 = null;
			
			// Obtenim el radi permés
			//float iDistKM = (float) Double.valueOf(SystemService.rec_valor("distkm", poEm)).doubleValue();
			
			T11infousr oT11infousr;
			T11UsrInfoBean oT11UsrInfoBean;
			boolean bObs = false;
			while (itRelusrT11.hasNext()){
				oRelusrT11 = (RelusrT11) itRelusrT11.next();
				// Es comprova si no estan obsoletes (aquesta funció, ja modifica la situació)
				bObs = es_obsoleta(oRelusrT11);
				if (bObs){
					oQuery = poEm.createNamedQuery("updateOB");
					oQuery.setParameter(1, oRelusrT11.getId());
					
					oQuery.executeUpdate();
				} else {
					// Es comprova si es un transport compatible
					oErrorBean = TransportsService.es_trans_compatible(oRelusrT11.getTransEmi(), psTrans, poEm);
					if ((oErrorBean != null) && (oErrorBean.getCodi().compareTo("") == 0)){
	//					if (dist((float) oRelusrT11.getLatEmi(), (float) oRelusrT11.getLngEmi(), (float) pdLat, (float) pdLng) <= iDistKM){
							oT11infousr = poEm.find(T11infousr.class, oRelusrT11.getAliasEMS());
							if (oT11infousr!= null){
								oT11UsrInfoBean = new T11UsrInfoBean();
								oT11UsrInfoBean.setAlias(oT11infousr.getAlias());
								oT11UsrInfoBean.setNumofr(oT11infousr.getNumofr());
								oT11UsrInfoBean.setNumrsvAcp(oT11infousr.getNumrsvAcp());
								oT11UsrInfoBean.setNumrsvCan(oT11infousr.getNumrsvCan());
								oT11UsrInfoBean.setNumtrt(oT11infousr.getNumtrt());
								oT11UsrInfoBean.setIdRelT11(oRelusrT11.getId());
								
								oListT11UsrInfoBean.getlT11UsrInfoBean().add(oT11UsrInfoBean);
							}
						//}
					}
				}
			}
			
			if (oListT11UsrInfoBean.getlT11UsrInfoBean().size() == 0)
				oListT11UsrInfoBean.setoErrorBean(new ErrorBean("SQL0001", "No existeixen T11 amb aquest criteri"));
			
		 } catch (NoResultException e) {
			oErrorBean = new ErrorBean("SQL0001", "No existeixen T11 oferides en el sistema");
			oListT11UsrInfoBean.setoErrorBean(oErrorBean);
		 }

		 return oListT11UsrInfoBean;
	 }
	  
	 public static ErrorBean reservar_T11( int piIDRel,
			 							   String psAlias, 
	     	    						   String psTrans,
	     	    						   double pdLat,
	     	    						   double pdLng,
	     	    						   EntityManager poEm){
		 ErrorBean  oErrorBean=null;
		 RelusrT11 oRelusrT11 = poEm.find(RelusrT11.class, piIDRel);
		 if (oRelusrT11 != null) {
			// Valida el Usuari/PassWord
			oErrorBean = UsuariService.validar_Usuari(psAlias, poEm);
			if ((oErrorBean != null) && (oErrorBean.getCodi().compareTo("") == 0)) {
				if (oRelusrT11.getAliasEMS().compareTo(psAlias) != 0) {
					if (oRelusrT11.getSit().compareTo(SitT11_OFERIDA) == 0) {
						
						// Es tornen a comprovar la validessa de la T11 a reservar per si de cas
						oErrorBean = TransportsService.es_trans_compatible(oRelusrT11.getTransEmi(), psTrans, poEm);
						if ((oErrorBean != null) && (oErrorBean.getCodi().compareTo("") == 0)){
					//		int iRadi = Integer.valueOf(SystemService.rec_valor("raddist", poEm)).intValue();
					//		if (dist((float)oRelusrT11.getLatEmi(), (float)oRelusrT11.getLngEmi(), (float)pdLat, (float) pdLng) <= iRadi){
								Query oQuery = poEm.createNamedQuery("updateDM");
								oQuery.setParameter(1, psAlias);
								oQuery.setParameter(2, psTrans);
								oQuery.setParameter(3, pdLat);
								oQuery.setParameter(4, pdLng);
								oQuery.setParameter(5, SitT11_DEMANADA);
								oQuery.setParameter(6, piIDRel);
								
								oQuery.executeUpdate();
								
								mod_T11infousr(psAlias, false, true, false, false, poEm);
					//		}
						} 
					} else oErrorBean = new ErrorBean("VLD0003", "T11 reservada per un altre usuari. Si us plau, escolli una altre");
				} else oErrorBean = new ErrorBean("VLD0003", "Operació no permessa, no es permet la reserva de la T11 el mateix usuari");
			}	
		 } else oErrorBean = new ErrorBean("VLD0003", "Identificador de relació no trobat"); 
		 
		 return oErrorBean;
	 }
	 
	 public static ErrorBean cancelar_T11( int piIDRel,
			 							   String psAlias, 
			 							   EntityManager poEm){
		ErrorBean  oErrorBean=null;
		RelusrT11 oRelusrT11 = poEm.find(RelusrT11.class, piIDRel);
		if (oRelusrT11 != null) {
			if  (oRelusrT11.getSit().compareTo(SitT11_DEMANADA) == 0) { 
				if	(oRelusrT11.getAliasRCP().compareTo(psAlias) == 0) 	{
					Query oQuery;
					if (!es_obsoleta(oRelusrT11)) {
						oQuery = poEm.createNamedQuery("updateCN");
						oQuery.setParameter(1, piIDRel);
						
						oQuery.executeUpdate();
						mod_T11infousr(psAlias, false, false, true, false, poEm);
						
						oErrorBean = new ErrorBean();
					} else {
						oQuery = poEm.createNamedQuery("updateOB");
						oQuery.setParameter(1, piIDRel);
						
						oQuery.executeUpdate();
						oErrorBean = new ErrorBean("VLD0003", "La T11 seleccionada està obsoleta");
					}
					
				} else oErrorBean = new ErrorBean("VLD0003", "El usuari introduït no coincideix amb l'usuari que ho va reservar");
			} else oErrorBean = new ErrorBean("VLD0003", "Problemes amb aquest registre T11, torni a cercar");
		} else 	oErrorBean = new ErrorBean("VLD0003", "Identificador de relació no trobat");
		
		return oErrorBean;
	 }
	 
	 public static ErrorBean confirmar_T11( int piIDRel,
			 							    String psAlias, 
			 							    EntityManager poEm){
		ErrorBean  oErrorBean=null;
		RelusrT11 oRelusrT11 = poEm.find(RelusrT11.class, piIDRel);
		if (oRelusrT11 != null) {
			if  ((oRelusrT11.getSit().compareTo(SitT11_DEMANADA) == 0)) { 
				if	(oRelusrT11.getAliasRCP().compareTo(psAlias) == 0)  {
					Query oQuery;
					if (!es_obsoleta(oRelusrT11)) {
						oQuery = poEm.createNamedQuery("updateTR");
						oQuery.setParameter(1, piIDRel);
						
						oQuery.executeUpdate();
						
						mod_T11infousr(psAlias, false, false, false, true, poEm);
					} else {
						oQuery = poEm.createNamedQuery("updateOB");
						oQuery.setParameter(1, piIDRel);
						
						oQuery.executeUpdate();
						
						oErrorBean = new ErrorBean("VLD0003", "La T11 seleccionada està obsoleta");
					}
				} else oErrorBean = new ErrorBean("VLD0003", "El usuari introduït no coincideix amb l'usuari que ho va reservar");
			} else oErrorBean = new ErrorBean("VLD0003", "Problemes amb aquest registre T11, torni a cercar");
		} else oErrorBean = new ErrorBean("VLD0003", "Identificador de relació no trobat");
		
		return oErrorBean;

	 }
	 
	 @SuppressWarnings("deprecation")
	public static ListRelUsrT11Bean Llistat_T11_Emissor( String psAlias, EntityManager poEm){
		ErrorBean  oErrorBean=null;
		ListRelUsrT11Bean oListRelUsrT11Bean = new ListRelUsrT11Bean();
		Date oDate;
		Time oTime;
		SimpleDateFormat oSimpleDateFormat = new SimpleDateFormat("yyyyMMdd");
		int iHora;
		int iMin;
		
		oErrorBean = UsuariService.validar_Usuari(psAlias, poEm);
		if ((oErrorBean != null) && (oErrorBean.getCodi().compareTo("") == 0)) {
			
			Query oQuery = poEm.createNamedQuery("getListT11EMS");
			oQuery.setParameter(1, psAlias);
			
			@SuppressWarnings("rawtypes")
			List loRelusrT11 = oQuery.getResultList();
				
			@SuppressWarnings("rawtypes")
			Iterator  itRelusrT11 = loRelusrT11.iterator();
						
			RelusrT11 oRelusrT11 = null;
			RelusrT11Bean oRelusrT11Bean;
			
			if (loRelusrT11.size()==0) {
				oErrorBean = new ErrorBean("SQL0001", "No existeixen T11 d'aquest usuari");
				oListRelUsrT11Bean.setoErrorBean(oErrorBean);
			}
			
			while (itRelusrT11.hasNext()){
				oRelusrT11 = (RelusrT11) itRelusrT11.next();
				
				oRelusrT11Bean = null;
				oRelusrT11Bean = new RelusrT11Bean();
				
				oRelusrT11Bean.setAliasEMS(oRelusrT11.getAliasEMS());
				oRelusrT11Bean.setAliasRCP(oRelusrT11.getAliasRCP());
				
				oDate = null;
				oTime = null;
		
				oDate = oRelusrT11.getDataVld();
				oTime = oRelusrT11.getHoraVld();
				
				String sDateVld = oSimpleDateFormat.format(oDate);
				
				oRelusrT11Bean.setDataVld(sDateVld);
			
				iHora = oTime.getHours();
				iMin = oTime.getMinutes();
							
				oRelusrT11Bean.setHoraVld(String.valueOf(iHora) + String.valueOf(iMin));
				oRelusrT11Bean.setId(oRelusrT11.getId());
				oRelusrT11Bean.setLatEmi(oRelusrT11.getLatEmi());
				oRelusrT11Bean.setLatRcp(oRelusrT11.getLatRcp());
				oRelusrT11Bean.setLngEmi(oRelusrT11.getLngEmi());
				oRelusrT11Bean.setLngRcp(oRelusrT11.getLngRcp());
				oRelusrT11Bean.setSit(oRelusrT11.getSit());
				oRelusrT11Bean.setTransEmi(oRelusrT11.getTransEmi());
				oRelusrT11Bean.setTransRcp(oRelusrT11.getTransRcp());
				oRelusrT11Bean.setMiss(oRelusrT11.getText());
	
				oListRelUsrT11Bean.getlRelusrT11Bean().add(oRelusrT11Bean);
			}
			
		} else  oListRelUsrT11Bean.setoErrorBean(oErrorBean);
		
		return oListRelUsrT11Bean;
	}
	 
	 @SuppressWarnings("deprecation")
	public static ListRelUsrT11Bean Llistat_T11_Receptor( String psAlias, EntityManager poEm){
		 ErrorBean  oErrorBean=null;
			ListRelUsrT11Bean oListRelUsrT11Bean = new ListRelUsrT11Bean();
			Date oDate;
			Time oTime;
			SimpleDateFormat oSimpleDateFormat = new SimpleDateFormat("yyyyMMdd");
			int iHora;
			int iMin;
			
			oErrorBean = UsuariService.validar_Usuari(psAlias, poEm);
			if ((oErrorBean != null) && (oErrorBean.getCodi().compareTo("") == 0)) {
				
				Query oQuery = poEm.createNamedQuery("getListT11RCP");
				oQuery.setParameter(1, psAlias);
				
				@SuppressWarnings("rawtypes")
				List loRelusrT11 = oQuery.getResultList();
					
				@SuppressWarnings("rawtypes")
				Iterator  itRelusrT11 = loRelusrT11.iterator();
							
				RelusrT11 oRelusrT11 = null;
				RelusrT11Bean oRelusrT11Bean;
				
				if (loRelusrT11.size()==0) {
					oErrorBean = new ErrorBean("SQL0001", "No existeixen T11 d'aquest usuari");
					oListRelUsrT11Bean.setoErrorBean(oErrorBean);
				}
				
				while (itRelusrT11.hasNext()){
					oRelusrT11 = (RelusrT11) itRelusrT11.next();
					
					oRelusrT11Bean = null;
					oRelusrT11Bean = new RelusrT11Bean();
					
					oRelusrT11Bean.setAliasEMS(oRelusrT11.getAliasEMS());
					oRelusrT11Bean.setAliasRCP(oRelusrT11.getAliasRCP());
					
					oDate = null;
					oTime = null;
			
					oDate = oRelusrT11.getDataVld();
					oTime = oRelusrT11.getHoraVld();
					
					String sDateVld = oSimpleDateFormat.format(oDate);
					
					oRelusrT11Bean.setDataVld(sDateVld);
				
					iHora = oTime.getHours();
					iMin = oTime.getMinutes();
								
					oRelusrT11Bean.setHoraVld(String.valueOf(iHora) + String.valueOf(iMin));
					oRelusrT11Bean.setId(oRelusrT11.getId());
					oRelusrT11Bean.setLatEmi(oRelusrT11.getLatEmi());
					oRelusrT11Bean.setLatRcp(oRelusrT11.getLatRcp());
					oRelusrT11Bean.setLngEmi(oRelusrT11.getLngEmi());
					oRelusrT11Bean.setLngRcp(oRelusrT11.getLngRcp());
					oRelusrT11Bean.setSit(oRelusrT11.getSit());
					oRelusrT11Bean.setTransEmi(oRelusrT11.getTransEmi());
					oRelusrT11Bean.setTransRcp(oRelusrT11.getTransRcp());
					oRelusrT11Bean.setMiss(oRelusrT11.getText());
		
					oListRelUsrT11Bean.getlRelusrT11Bean().add(oRelusrT11Bean);
				}
				
			} else  oListRelUsrT11Bean.setoErrorBean(oErrorBean);
			
			return oListRelUsrT11Bean;
	}
	 
	 @SuppressWarnings("deprecation")
	 private static boolean es_obsoleta(RelusrT11 poRelusrT11){
		 boolean bRet = false; 
		 SimpleDateFormat oSimpleDateFormat = new SimpleDateFormat("yyyyMMdd");
		 Calendar oCalVld = Calendar.getInstance();
		 Calendar oCalAct = new GregorianCalendar();
		 Date oDate = poRelusrT11.getDataVld();
		 Time oTime = poRelusrT11.getHoraVld();
		
		 String sDateVld = oSimpleDateFormat.format(oDate);
		 
		 int iYear = Integer.valueOf(sDateVld.substring(0, 4)).intValue();
		 int iMonth = Integer.valueOf(sDateVld.substring(4, 6)).intValue();
		 int iDay = Integer.valueOf(sDateVld.substring(6, 8)).intValue();
		 int iHora = oTime.getHours();
		 int iMin = oTime.getMinutes();
		 
		 int iYearAct = oCalAct.get(Calendar.YEAR);
		 int iMesAct = oCalAct.get(Calendar.MONTH);
		 int iDiaAct = oCalAct.get(Calendar.DAY_OF_MONTH);
		 
		 int iHoraTMP = oCalAct.get(Calendar.HOUR_OF_DAY);
		 int iMinTMP = oCalAct.get(Calendar.MINUTE);
		 
		 // Es recupera la data actual exacta
		 oCalAct.set(iYearAct, iMesAct + 1, iDiaAct, iHoraTMP, iMinTMP);
		 
		 oCalVld.set(iYear, iMonth, iDay, iHora, iMin);
		 
		 long lMilisAct = oCalAct.getTimeInMillis();
		 long lMilisVld = oCalVld.getTimeInMillis();
		 
		 long lMilisDiff = lMilisAct - lMilisVld;
		 
		 long lSegDiff = lMilisDiff / 1000;
		 
		 if (lSegDiff > (2.30 * 60 * 60))
			 bRet = true;
		 
		 return bRet;
	 }
	 
	 /*
	 private static double dist(float dLatEMS, float dLngEMS, float dLatRCP, float dLngRCP){
		 float R =  (float) 6378.137;
		 float dLat = rad(dLatRCP - dLatEMS);
		 float dLong = rad(dLngRCP - dLngEMS);
		 
		 float da = (float) (Math.sin(dLat/2) * Math.sin(dLat/2) + Math.cos(rad(dLatEMS)) * Math.cos(rad(dLatRCP)) * Math.sin(dLong/2) * Math.sin(dLong/2));
		 float dc = (float) (2 * Math.atan2(Math.sqrt(da), Math.sqrt(1-da)));
		 float dd = R * dc;
		 
		 return dd;
	 }
	 
	 private static float rad(float dValor){
		 float dRet = 0;
		 
		 dRet = (float) (dValor*Math.PI/180);
		 
		 return dRet;
	 }
*/	 
	 private static void mod_T11infousr(String psAlias, boolean bNumOfer, boolean bNumrsvAcp, boolean bNumrsvCan, boolean bNumTrt, EntityManager poEm){
		 T11infousr oT11infousr = poEm.find(T11infousr.class, psAlias);
		 if(oT11infousr == null){
			oT11infousr = new T11infousr();
			oT11infousr.setAlias(psAlias);
			oT11infousr.setNumofr(0);
			oT11infousr.setNumrsvAcp(0);
			oT11infousr.setNumrsvCan(0);
			oT11infousr.setNumtrt(0);
			poEm.persist(oT11infousr);
		 }
		 
		 int iNumOfer = oT11infousr.getNumofr();
		 int iNumrsvAcp = oT11infousr.getNumrsvAcp();
		 int iNumrsvCan = oT11infousr.getNumrsvCan();
		 int iNumTrt = oT11infousr.getNumtrt();
		 
		 if (bNumOfer) iNumOfer++;
		 if (bNumrsvAcp) iNumrsvAcp++;
		 if (bNumrsvCan) iNumrsvCan++;
		 if (bNumTrt) iNumTrt++;
		 
		 oT11infousr.setNumofr(iNumOfer);
		 oT11infousr.setNumrsvAcp(iNumrsvAcp);
		 oT11infousr.setNumrsvCan(iNumrsvCan);
		 oT11infousr.setNumtrt(iNumTrt);
		 
		 poEm.refresh(oT11infousr);
		 poEm.flush();
	 }
}
