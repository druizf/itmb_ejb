package com.iTMB.bean;

public interface iTMBInterfaceService {
	
	// Usuari
	public ErrorBean validar_Usuari(String psAlias, String psPasswd);
	public ErrorBean crear_Usuari(String psAlias, String psPasswd, String psCorreu, String psNom, String psCognoms, int piDatanai, String psSexe, String psTelf);
	public ErrorBean modificar_Usuari(String psAlias, String psPasswd, String psCorreu, String psNom, String psCognoms, int piDatanai, String psSexe, String psTelf);
	public DadesusrBean obtenir_dades_Usuari(String psAlias);
	public ErrorBean esborrar_Usuari(String psAlias);
	
	// Transports
	public ZonaBean obtenir_Zones();
	public ErrorBean es_Zona(String psZona);
	public TipTransBean obtenir_Tip_Transports();
	public ErrorBean es_Tip_Transports(String psTrans);
	public ErrorBean es_trans_compatible(String psTrans1, String psTrans2);
	
	// Servei T11
	public ErrorBean oferir_T11(String psAlias, 
				                String psTrans,  
						        String psZona,
						        String psDataFnl,
						        String ptHoraFnl,
						        double pdLat,
						        double pdLng,
						        String psMiss
						        );
	
	 public ListT11UsrInfoBean disponibles_T11(String psTrans, double pdLat, double pdLng);
	
	 public ErrorBean reservar_T11(int piIDRel, String psAlias, String psTrans, double pdLat, double pdLng);
	 
	 public ErrorBean cancelar_T11(int piIDRel, String psAlias);
	 
	 public ErrorBean confirmar_T11(int piIDRel, String psAlias);
	 
	 public ListRelUsrT11Bean Llistat_T11_Emissor(String psAlias);
	 
	 public ListRelUsrT11Bean Llistat_T11_Receptor(String psAlias);
}
