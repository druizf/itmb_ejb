package com.iTMB.bean;

import java.io.Serializable;

public class RelusrT11Bean implements Serializable {
	private static final long serialVersionUID = 1L;
	
	private int id;

	private String aliasEMS;

	private String aliasRCP;

	private String dataVld;

	private String horaVld;

	private double latEmi;

	private double latRcp;

	private double lngEmi;

	private double lngRcp;

	private String sit;

	private String transEmi;

	private String transRcp;

	private byte zonaEmi;
	
	private String miss;

	public RelusrT11Bean() {
	}

	public int getId() {
		return this.id;
	}

	public String getMiss() {
		return miss;
	}

	public void setMiss(String miss) {
		this.miss = miss;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getAliasEMS() {
		return this.aliasEMS;
	}

	public void setAliasEMS(String aliasEMS) {
		this.aliasEMS = aliasEMS;
	}

	public String getAliasRCP() {
		return this.aliasRCP;
	}

	public void setAliasRCP(String aliasRCP) {
		this.aliasRCP = aliasRCP;
	}

	public String getDataVld() {
		return this.dataVld;
	}

	public void setDataVld(String dataVld) {
		this.dataVld = dataVld;
	}

	public String getHoraVld() {
		return this.horaVld;
	}

	public void setHoraVld(String horaVld) {
		this.horaVld = horaVld;
	}

	public double getLatEmi() {
		return this.latEmi;
	}

	public void setLatEmi(double latEmi) {
		this.latEmi = latEmi;
	}

	public double getLatRcp() {
		return this.latRcp;
	}

	public void setLatRcp(double latRcp) {
		this.latRcp = latRcp;
	}

	public double getLngEmi() {
		return this.lngEmi;
	}

	public void setLngEmi(double lngEmi) {
		this.lngEmi = lngEmi;
	}

	public double getLngRcp() {
		return this.lngRcp;
	}

	public void setLngRcp(double lngRcp) {
		this.lngRcp = lngRcp;
	}

	public String getSit() {
		return this.sit;
	}

	public void setSit(String sit) {
		this.sit = sit;
	}

	public String getTransEmi() {
		return this.transEmi;
	}

	public void setTransEmi(String transEmi) {
		this.transEmi = transEmi;
	}

	public String getTransRcp() {
		return this.transRcp;
	}

	public void setTransRcp(String transRcp) {
		this.transRcp = transRcp;
	}

	public byte getZonaEmi() {
		return this.zonaEmi;
	}

	public void setZonaEmi(byte zonaEmi) {
		this.zonaEmi = zonaEmi;
	}

}