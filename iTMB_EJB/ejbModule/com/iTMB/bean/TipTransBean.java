package com.iTMB.bean;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Iterator;

public class TipTransBean implements Serializable {
	
	private static final long serialVersionUID = 1L;

	ArrayList<String> tipTrans;

	ErrorBean oErrorBean;

	public TipTransBean() {
		super();
		
		this.tipTrans= new ArrayList<String>();
		this.oErrorBean = new ErrorBean();
	}

	public ArrayList<String> getTipTrans() {
		return this.tipTrans;
	}

	public void setTipTrans(String psTipTrans) {
		if (this.tipTrans != null)
			this.tipTrans.add(psTipTrans);
	}
	
	public ErrorBean getErrorBean() {
		return this.oErrorBean;
	}
	
	public void setErrorBean(ErrorBean oErrorBean) {
		this.oErrorBean = oErrorBean;
	}

	
	public String toString(){
		String sRet = null;
		String sTipTrans = null;
		
		sRet = "Tipus Transports: \n";
		
		@SuppressWarnings("rawtypes")
		Iterator itTipusTrans = this.tipTrans.iterator();
		
		while (itTipusTrans.hasNext()){
			sTipTrans = (String) itTipusTrans.next();
			sRet = sRet + sTipTrans + "\n";
		}
		
		sRet = sRet + "Error:"        +                        "\n" +
			          "Codi: "        + oErrorBean.getCodi() + "\n" +
					  "Descripció: "  + oErrorBean.getDescripcio() + "\n";
	
		return sRet;
	}
}