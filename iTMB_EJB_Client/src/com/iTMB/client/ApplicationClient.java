package com.iTMB.client;

import java.util.Properties;

import com.iTMB.bean.DadesusrBean;
import com.iTMB.bean.ErrorBean;
import com.iTMB.bean.ListRelUsrT11Bean;
import com.iTMB.bean.ListT11UsrInfoBean;
import com.iTMB.bean.ZonaBean;
import com.iTMB.bean.ZonaBean;
import com.iTMB.bean.TipTransBean;
import com.iTMB.bean.iTMBInterfaceService;

import javax.naming.Context;
import javax.naming.InitialContext;

public class ApplicationClient {
	 public static void main(String [] args)
	 {
		 try
		 {
			 Properties prop=new Properties();
			 prop.put(Context.INITIAL_CONTEXT_FACTORY, "org.apache.openejb.client.RemoteInitialContextFactory");
			 //prop.put("java.naming.provider.url", "ejbd://t11assistant.no-ip.org:4201");
			 prop.put("java.naming.provider.url", "ejbd://localhost:4201");
			 Context context = new InitialContext(prop);

			 iTMBInterfaceService oUsuariBean = (iTMBInterfaceService) context.lookup("iTMBServiceRemote");
			 
			 //ErrorBean oErrorBean = null;
			 //DadesusrBean oDadesusrBean = null;
			 
			 // validar_usuari
			 //oErrorBean = oUsuariBean.validar_Usuari("DaniRF", "pepe");
			 
			 // crear_usuari
			 //oErrorBean = oUsuariBean.crear_Usuari("DaniRF","pepe", "dani.ruiz.ferrer@gmail.com", "Dani", "Ruiz Ferrer", 28061981, "H", "647911479");
			 
			 // Obtenir_dades
			 //oDadesusrBean = oUsuariBean.obtenir_dades_Usuari("Adriatt");
			 //System.out.println(oDadesusrBean.toString());
			 
			 // modificar_usuari
			 //oErrorBean = oUsuariBean.modificar_Usuari("DaniRF","yoquese", "", "", "", 0, "", "");
			 
			// modificar_usuari
			//oErrorBean = oUsuariBean.esborrar_Usuari("DaniRF");
			 
			 //System.out.println(oErrorBean.toString());
			 
			 // Transports
			 //Llistar Zones
			 //ZonaBean oZonaBean2 = oUsuariBean.obtenir_Zones();
			 //System.out.println(oZonaBean2.toString());
			 
			 //Llistar TipusTransports
			 //TipTransBean oTipTransBean = oUsuariBean.obtenir_Tip_Transports();
			 
			 //System.out.println(oTipTransBean.toString());
			 
			 // Transport compatible
			 //System.out.println(oUsuariBean.es_trans_compatible("kk", "FGC"));
			 
			 ListRelUsrT11Bean oListT11UsrInfoBean =  oUsuariBean.Llistat_T11_Emissor("DaniRF");
			 
			 
			 System.out.println(oListT11UsrInfoBean.getlRelusrT11Bean().get(0).getDataVld());
			 
		 }
		 catch(Exception e)
		 {
			 e.printStackTrace();
		 }
	 }


}